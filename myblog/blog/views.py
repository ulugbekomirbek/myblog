import json
from django.core.paginator import Paginator
from django.core.serializers.json import DjangoJSONEncoder
from django.http import HttpResponse
from django.shortcuts import get_object_or_404, render
from django.utils import timezone
from django.views import generic
from blog.models import Posting, Comment


def json_feed(request):
    data = [posting.json() for posting in Posting.objects.all().order_by(
        '-posting_date')]
    jsonString = json.dumps(data, cls=DjangoJSONEncoder, indent=4)
    with open('blog/rss-json/data.json', 'w') as output:
        json.dump(data, output, cls=DjangoJSONEncoder, indent=4)
    return HttpResponse(jsonString, content_type='application/json')


class IndexView(generic.ListView):
    template_name = 'blog/index.html'
    context_object_name = 'latest_posting_list'
    paginate_by = 5

    def get_queryset(self):
        objects = Posting.objects.all().order_by('-posting_date')
        return objects


class DetailView(generic.DetailView):
    model = Posting
    template_name = 'blog/detail.html'

    def get_queryset(self):
        return Posting.objects.all()


def comment(request, posting_id):
    posting = get_object_or_404(Posting, pk=posting_id)
    p = Posting.objects.get(pk=posting_id)
    user_comment = request.POST['comment_text']
    if not user_comment:
        return render(
            request,
            'blog/detail.html',
            {
                'posting': posting,
                'error_message': "You didn't put a comment!",
            }
         )
    else:
        p.comment_set.create(
            comment_text=user_comment, comment_date=timezone.now())
        p.save()
        return render(
           request,
           'blog/detail.html',
           {
               'posting': posting,
               'success_message': "Your comment has been added!",
            }
        )
