import datetime
from django.db import models
from django.utils import timezone


class Posting(models.Model):
    posting_topic = models.CharField(max_length=50, default="New Post")
    posting_text = models.TextField(max_length=2000)
    posting_date = models.DateTimeField('date posted', auto_now_add=True)
    update_date = models.DateTimeField(
        'date updated',
        auto_now=True,
        blank=True,
        null=True)

    def __str__(self):
        return self.posting_topic

    def json(self):
        return {
            'posting_topic': self.posting_topic,
            'posting_text': self.posting_text,
            'posting_date': self.posting_date,
            'update_date': self.update_date,
            'comment': [
                comment.json() for comment in Comment.objects.filter(
                    posting=self).order_by('comment_date')],
            }


class Comment(models.Model):
    posting = models.ForeignKey(Posting, on_delete=models.CASCADE)
    comment_text = models.CharField(max_length=500)
    comment_date = models.DateTimeField('date commented')

    def __str__(self):
        return self.comment_text

    def json(self):
        return {
            'comment_text': self.comment_text,
            'comment_date': self.comment_date,
            }
