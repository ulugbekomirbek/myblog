from django.conf.urls import url

from blog import views

app_name = 'blog'
urlpatterns = [
    url(r'^$', views.IndexView.as_view(), name='index'),
    url(r'^(?P<pk>[0-9]+)/$', views.DetailView.as_view(), name='detail'),
    url(r'^(?P<posting_id>[0-9]+)/comment/$', views.comment, name='comment'),
    url(r'^json/$', views.json_feed, name='json_feed'),
    ]
