# -*- coding: utf-8 -*-
# Generated by Django 1.9.5 on 2016-04-12 06:39
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('blog', '0002_posting_posting_topic'),
    ]

    operations = [
        migrations.AlterField(
            model_name='posting',
            name='posting_text',
            field=models.CharField(max_length=200),
        ),
    ]
