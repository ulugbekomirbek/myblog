$(document).ready(function(){
  $("#show").hide();
  $("#back").click(function(){
     var path = location.origin+"/blog/";
     window.location = path;
  });
  $("#hide").click(function(){
    $("#commenting").hide();
    $("#hide").hide();
    $("#show").show();
  });
  $("#show").click(function(){
    $("#commenting").show();
    $("#hide").show();
    $("#show").hide();
  });
});
